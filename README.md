# TkTextToSpeech

Text To Speech using pyttsx3 and Tkinter for the GUI

![Screenshot](img/example.png)
## Install and Run

Requires: Python3, pyttsx3, Tkinter.

Run app.py to start application

```
pip install -r requirements.txt

python3 app.py
```

