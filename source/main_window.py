import tkinter as tk


class MainWindow:
	def __init__(self):
		# window
		self._window = tk.Tk()
	
		self._window.title("Text To Speech")
		self._window.geometry("660x260")
	
		# Text
		label_text = tk.Label(text="Enter Text To Convert To Speech:")
	
		self._text_text = tk.Text(wrap=tk.WORD)
	
		# Save
		label_save = tk.Label(text="Save to:")
	
		self._entry_save = tk.Entry()
	
		# Rate
		label_rate = tk.Label(text="Rate:")
	
		var = tk.IntVar()
		self._scale_rate = tk.Scale(orien=tk.HORIZONTAL, variable=var, from_=1, to=240)
		self._scale_rate.set(120)
	
		# Volume
		label_volume = tk.Label(text="Volume:")
	
		var = tk.DoubleVar()
		self._scale_volume = tk.Scale(orien=tk.HORIZONTAL, variable=var, from_=1, to=100)
		self._scale_volume.set(100)
	
		# Save 
		self._button_save = tk.Button(text="Save & Close", padx=6, pady=6)
		
		# Play
		self._button_play = tk.Button(text="Play", padx=6, pady=6)
		
		# Style
		self._window.columnconfigure(0, weight=1, minsize=75)
		self._window.columnconfigure(1, weight=1, minsize=75)
		self._window.rowconfigure(1, weight=1, minsize=50)

		label_text.grid(row=0, column=0, stick="nsew")
		self._text_text.grid(row=1, column=0, columnspan=2, 
							stick="nsew", padx= 6, pady=6)
		
		label_save.grid(row=2, column=0, stick="nsew", padx= 6, pady=6)
		self._entry_save.grid(row=2, column=1, stick="nsew", padx= 6, pady=6)
		
		label_rate.grid(row=3, column=0, stick="nsew", padx= 6, pady=6)
		self._scale_rate.grid(row=3, column=1, stick="nsew", padx= 6, pady=6)
		
		
		label_volume.grid(row=4, column=0, stick="nsew", padx= 6, pady=6)
		self._scale_volume.grid(row=4, column=1, stick="nsew", padx= 6, pady=6)
		
		self._button_save.grid(row=5, column=0, stick="nsew", padx= 6, pady=6)
		self._button_play.grid(row=5, column=1, stick="nsew", padx= 6, pady=6)
	
	def commands(self, commands):
		self._button_save.config(command=lambda: commands[0](self._text_text.get("1.0", "end"), self._entry_save.get(), self._scale_rate.get(), self._scale_volume.get()))
		self._button_play.config(command=lambda: commands[1](self._text_text.get("1.0", "end"), self._scale_rate.get(), self._scale_volume.get()))
	
	def start(self):
		self._window.mainloop()
		
	def stop(self):
		self._window.destroy()
			
	def window_response(message="Empty Response"):
		window_response = tk.Window()
		window.title(message.split(" ")[0])
		
		label_response = tk.Label(text=message)
		
		label_response.pack()
