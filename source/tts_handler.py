import pyttsx3 as tts


engine = tts.init()


def to_audio(text="",
             filepath="default.mp3",
             rate=120,
             volume=1.0,
             save=True):
    engine.setProperty('rate', rate)
    engine.setProperty('volume', volume)

	# FIXME: Not Changing Voice As Expected
    # voices = engine.getProperty('voices')
    # engine.setProperty('voices', voices[13])

    if save:
		# FIXME: Save Only Works Once Then Hangs On 2nd Attempt
        engine.save_to_file(text, filepath)
    else:
        engine.say(text)

    engine.runAndWait()
    engine.stop()
