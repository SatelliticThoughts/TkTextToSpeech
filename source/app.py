import main_window as mw
import tts_handler as tts


def save_speech(text, filepath, rate, volume):
    tts.to_audio(text=text,
                 filepath=filepath,
                 rate=rate,
                 volume=float(volume)/100.0)
    window.stop()


def play_speech(text, rate, volume):
    tts.to_audio(text=text, rate=rate, volume=float(volume)/100.0, save=False)


if __name__ == "__main__":
    window = mw.MainWindow()
    window.commands([save_speech, play_speech])
    window.start()
